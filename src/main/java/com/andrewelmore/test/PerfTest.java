package com.andrewelmore.test;


import java.lang.management.GarbageCollectorMXBean;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public class PerfTest {

    private static final int NUM_THREADS = 4;
    private static final long WARMUP = 10000L;
    private static final long RUNTIME = 60000L;
    private static final int MAX_ITERATIONS_PER_THREAD = 000;

    
    private static void init() {
    	// Place any initialisation code here
    }
    
    private static Worker createWorker() {
    	// Instantiate a worker here
        return null;
    }
    	
    public static void main(String[] args) throws Exception {

    	// ------------------------
    	// Prepare
    	// ------------------------
    	
    	init();

        Thread[] threads = new Thread[NUM_THREADS];
        Worker[] worker = new Worker[NUM_THREADS];
        
        for (int i = 0; i < NUM_THREADS; i++) {
        	worker[i] = createWorker();
        	if(MAX_ITERATIONS_PER_THREAD > 0) {
                worker[i].setLimit(MAX_ITERATIONS_PER_THREAD);
        	}
            threads[i] = new Thread(worker[i]);
        }
        
        // ------------------------
        // Warm-up
        // Attempt to more closely mirror what steady-state will look like for our timed run
        // ------------------------

        System.out.println("Warmup Start - " + WARMUP + " ms");
        
        for (int i = 0; i < NUM_THREADS; i++) {
            threads[i].start();
        }
        
        Thread.sleep(WARMUP);
        
        // Encourage GC
        System.gc();

        System.out.println("Warmup Completed");
        
        // ------------------------
        // Timed run
        // ------------------------

        System.out.println("Run Start");

        
        // Record the start GC time
        long gcTime = 0;
        for(GarbageCollectorMXBean bean : ManagementFactory.getGarbageCollectorMXBeans()) {
        	gcTime -= bean.getCollectionTime();
        }
        
        // Reset the worker threads
        for (int i = 0; i < NUM_THREADS; i++) {
            worker[i].reset();
        }
        
        // Allow the test to run for the defined period
        if(MAX_ITERATIONS_PER_THREAD <= 0) {
	        // Shutdown and let last thread finish
	        Thread.sleep(RUNTIME);
	        for (int i = 0; i < NUM_THREADS; i++) {
	            worker[i].stop();
	        }
        }

        // Wait for the threads to complete
        for (int i = 0; i < NUM_THREADS; i++) {
            threads[i].join();
            System.out.println("Thread " + i + ": " + worker[i].getCount() + " iterations in " + worker[i].getRuntime() + " cpu ns");
        }

        System.out.println("Run Completed");
        
        // Encourage GC
        System.gc();

        // Give it time to do something if it wants to
        Thread.sleep(500);
        
        // Record how much garbage collection time we 'created' with the test
        // This is at best an estimate as the VM might chose to not GC
        for(GarbageCollectorMXBean bean : ManagementFactory.getGarbageCollectorMXBeans()) {
        	gcTime += bean.getCollectionTime();
        }

        // Total up what we did and in how long
        int transactions = 0;
        long totalCpuTime = 0;
        for (int i = 0; i < NUM_THREADS; i++) {
            transactions += worker[i].getCount();
            totalCpuTime += worker[i].getRuntime();
        }

        double cpuSecs = ((double) totalCpuTime) / 1000000000L;
        double gcSecs = (((double)gcTime) / 1000);
        
        
        System.out.println("Runtime (secs): " + ((double)RUNTIME) / 1000);
        System.out.println("Iterations: " + transactions);
        System.out.println("Average CPU time per thread (secs): " + cpuSecs / NUM_THREADS);
        System.out.println("GC Duration (secs): " + gcSecs);
        System.out.println("Tx/sec: " + (transactions / ((double)RUNTIME / 1000)));
        System.out.println("Tx/cpusec: " + (transactions / (cpuSecs / NUM_THREADS)));


    }
    
    
    
    static abstract class Worker implements Runnable {
        protected String msgStr = null;
        private volatile int count = 0;
        private int limit = 0;
        private volatile long runtime = 0;
        private volatile boolean running = true;
        private volatile boolean reset = false;
        private volatile boolean warmup = true;
        private long startTime = System.currentTimeMillis();
        private ThreadMXBean mxBean = ManagementFactory.getThreadMXBean();

        public int getCount() {
            return count;
        }
        
        public long getRuntime() {
                return runtime;
        }
        
        public void reset() {
                reset = true;
        }
        
        public void stop() {
                running = false;
        }
        
        public void setLimit(int limit) {
        	this.limit = limit;
        }
        
        public abstract void process() throws Exception;
        
        public void run() {
        	if(!mxBean.isCurrentThreadCpuTimeSupported()) {
        		System.out.println("CPU Time not supported; using wall clock time");
        	}

            while (warmup || (running && (limit == 0 || count < limit))) {
                try {
                	process();
                    if(reset) {
                        count = 0;
                        startTime = mxBean.isCurrentThreadCpuTimeSupported()? mxBean.getCurrentThreadCpuTime() : System.nanoTime();
                        reset = false;
                        warmup = false;
                    } else {
                        count++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            runtime = (mxBean.isCurrentThreadCpuTimeSupported()? mxBean.getCurrentThreadCpuTime() : System.nanoTime()) - startTime;
        }
    }
    



}
